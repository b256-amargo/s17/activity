console.log("Hello World!");

/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:
	function funcOne() {
		let userName = prompt("What is your name?");
		let userAge = prompt("Hwo old are you?");
		let userAddress = prompt("Where do you live?");

		alert("Thank you for your input!");

		console.log("Hello, " + userName);
		console.log("You are " + userAge + " years old.");
		console.log("You live in " + userAddress);
	}
	funcOne();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
	function funcTwo() {
		let userFavBand_1 = "1. Tenjin Kotone"
		let userFavBand_2 = "2. Hoshimachi Suisei"
		let userFavBand_3 = "3. kessoku band"
		let userFavBand_4 = "4. nanawo akari"
		let userFavBand_5 = "5. Eraserheads"

		console.log(userFavBand_1);
		console.log(userFavBand_2);
		console.log(userFavBand_3);
		console.log(userFavBand_4);
		console.log(userFavBand_5);
	}
	funcTwo();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
	function funcThree() {
		let userFavMovie_title1 = "1. Maquia: When the Promised Flower Blooms", userFavMovie_title2 = "2. The Girl Who Leapt Through Time", userFavMovie_title3 = "3. The Lord of the Rings: The Return of the King", userFavMovie_title4 = "4. The Martian", userFavMovie_title5 = "5. Interstellar";
		let userFavMovie_rating1 = "100%", userFavMovie_rating2 = "84%", userFavMovie_rating3 = "93%", userFavMovie_rating4 = "91%", userFavMovie_rating5 = "72%";

		function printMovie1() {
			console.log(userFavMovie_title1);
			console.log("Rotten Tomatoes Rating: " + userFavMovie_rating1);
		};
		function printMovie2() {
			console.log(userFavMovie_title2);
			console.log("Rotten Tomatoes Rating: " + userFavMovie_rating2);
		};
		function printMovie3() {
			console.log(userFavMovie_title3);
			console.log("Rotten Tomatoes Rating: " + userFavMovie_rating3);
		};
		function printMovie4() {
			console.log(userFavMovie_title4);
			console.log("Rotten Tomatoes Rating: " + userFavMovie_rating4);
		};
		function printMovie5() {
			console.log(userFavMovie_title5);
			console.log("Rotten Tomatoes Rating: " + userFavMovie_rating5);
		};
		
		printMovie1();
		printMovie2();
		printMovie3();
		printMovie4();
		printMovie5();
	};
	funcThree();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

// printUsers();
let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:");
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};
printFriends();

// console.log(friend1);
// console.log(friend2);

